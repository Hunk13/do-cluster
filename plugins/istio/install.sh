#!/bin/bash

# Install the crds
for i in crds/crd*yaml; do kubectl apply -f $i; done

# Sleep for 20 seconds till the crds are up
sleep 20

# Apply istio 
kubectl apply -f istio-demo.yaml
