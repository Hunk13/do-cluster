#!/bin/bash

# INITIALIZE TILLER 

helm init

# UPDATE HELM REPO

helm repo update

# ENABLE RBAC FOR HELM

kubectl create serviceaccount --namespace kube-system tiller

kubectl create clusterrolebinding tiller-cluster-rule --clusterrole=cluster-admin --serviceaccount=kube-system:tiller

kubectl patch deploy --namespace kube-system tiller-deploy -p '{"spec":{"template":{"spec":{"serviceAccount":"tiller"}}}}' 

# SAMPLE SYNTAX TO INSTALL MYSQL

   ## helm install stable/mysql --name my-special-installation --set mysqlPassword=johnjose
