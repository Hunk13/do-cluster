FROM node:8

RUN mkdir -p /usr/src/app

WORKDIR /usr/src/app

ADD package.json /usr/src/app/package.json
RUN npm install --production
ADD src/index.js /usr/src/app/index.js

ENTRYPOINT ["node", "index.js"]
