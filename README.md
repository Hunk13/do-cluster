1.  kubectl create -f mongodb.yaml
2.  kubectl exec -it mongo-0 -- mongo
3.  rs.initiate()
4.  var cfg = rs.conf();cfg.members[0].host="mongo-0.mongo:27017";rs.reconfig(cfg) 
5.  rs.add("mongo-0.mongo:27017")
6.  rs.add("mongo-1.mongo:27017")
7.  kubectl label namespace default istio-injection=enabled
8.  skaffold run
9.  kubectl port-forward mongo-0 5432:27017 --address 0.0.0.0 (To connect to mongodb from local use: ```mongo localhost:5432```)
10. kubectl port-forward pod/prisma-mgmt-f8476bf7c-rzsfv 4466:4466 --address 0.0.0.0


------------------------------------------------------------------------------------------
Notes: 

STEP 1:

# To run the mongodb replicaSet

```
kubectl exec -it mongo-0 -- mongo
```

Initiate the MongoDB replica set:

```
rs.initiate()
```

Reconfigure the first member of the replica set with the correct DNS name:

```
var cfg = rs.conf();cfg.members[0].host="mongo-0.mongo:27017";rs.reconfig(cfg)

```

Add the remaining replica set nodes:

```
rs.add("mongo-0.mongo:27017")
rs.add("mongo-1.mongo:27017")
```

# Connecting to mongodb from your laptop

```
kubectl port-forward mongo-0 5432:27017 --address 0.0.0.0
```

then use Studio 3T for Mongodb and use

```
server: localhost
port: 5432

```
STEP 2:
-----------------------------------------------------------------------------------------------

# You will have to run the prisma server 

To run the prisma server, run the prisma management pod.

Note: Do not run the prisma server pod because prisma management pod routes the traffic to the prisma server pod

```
kubectl port-forward <pod name> <service-port>:<service-port> --address 0.0.0.0
kubectl port-forward pod/prisma-mgmt-f8476bf7c-rzsfv 4466:4466 --address 0.0.0.0
```

After starting the prisma server, then run ```prisma deploy```

prisma server url: http://localhost:4466


STEP 3:
---------------------------------------------------------------------------------------------------
# Prisma client end point example

Your Prisma endpoint is live:

  HTTP:  http://localhost:4466/microservice1/dev
  WS:    ws://localhost:4466/microservice1/dev

You can view & edit your data here:

  Prisma Admin: http://localhost:4466/microservice1/dev/_admin

.env file is required
PRISMA_MANAGEMENT_API_SECRET=johnjose
