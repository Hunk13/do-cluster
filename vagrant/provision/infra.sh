# INSTALL DOCKER (REQUIRED FOR SKAFFOLD)
     curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
     sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"
     sudo apt-get update
     sudo apt-get install -y docker-ce 
     sudo usermod -aG docker vagrant

# NODEJS
    sudo apt-get update && sudo apt-get -y upgrade
    curl -sL https://deb.nodesource.com/setup_8.x -o nodesource_setup.sh
    sudo bash nodesource_setup.sh
    sudo apt-get install nodejs -y
    sudo apt-get install build-essential -y

# PRISMA CLI FOR PRISMA SERVER
    sudo npm install -g prisma
  
# KUBECTL
    curl -LO https://storage.googleapis.com/kubernetes-release/release/$(curl -s https://storage.googleapis.com/kubernetes-release/release/stable.txt)/bin/linux/amd64/kubectl
    chmod +x ./kubectl
    sudo mv ./kubectl /usr/local/bin/kubectl

# SKAFFOLD
    curl -Lo skaffold https://storage.googleapis.com/skaffold/releases/latest/skaffold-linux-amd64
    chmod +x skaffold
    sudo mv skaffold /usr/local/bin

# HELM

    wget https://get.helm.sh/helm-v2.14.3-linux-amd64.tar.gz
    tar -xvzf helm-v2.14.3-linux-amd64.tar.gz
    sudo mv linux-amd64/helm /usr/local/bin
    rm helm-v2.14.3-linux-amd64.tar.gz
    rm -rf ./linux-amd64/

# ISTIO
    
    curl -L https://git.io/getLatestIstio | ISTIO_VERSION=1.2.5 sh -
    sudo mv istio-1.2.5/bin/istioctl /usr/local/bin
    rm -rf ./istio-1.2.5
